package com.example.keras.android;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.keras.android", appContext.getPackageName());
    }

    @Test
    public void tensorflowTest() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        TensorFlowInferenceInterface tensorflow = new TensorFlowInferenceInterface(
                appContext.getAssets(),
                "file:///android_asset/frozen_xor_nn.pb"
        );

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                float[] input = {i, j};
                float[] output = predict(input, tensorflow);

                Log.d("TESTANDROID", Arrays.toString(input) + " -> " + Arrays.toString(output));
            }
        }
    }

    @Test
    public void liteTensorflowTest() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        TensorFlowInferenceInterface tensorflow = new TensorFlowInferenceInterface(
                appContext.getAssets(),
                "file:///android_asset/tensorflow_lite_xor_nn.pb"
        );

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                float[] input = {i, j};
                float[] output = predict(input, tensorflow);

                Log.d("TESTANDROID", Arrays.toString(input) + " -> " + Arrays.toString(output));
            }
        }
    }

    private float[] predict(float[] input, TensorFlowInferenceInterface inferenceInterface) {
        // model has only 1 output neuron
        float output[] = new float[1];

        // feed network with input of shape (1,input.length) = (1,2)
        inferenceInterface.feed("dense_1_input", input, 1, input.length);
        inferenceInterface.run(new String[]{"dense_2/Sigmoid"});
        inferenceInterface.fetch("dense_2/Sigmoid", output);

        // return prediction
        return output;
    }

    @Test
    public void kerasTensorflowTest() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        TensorFlowInferenceInterface tensorflow = new TensorFlowInferenceInterface(
                appContext.getAssets(),
                "file:///android_asset/tensorflow_lite_keras.pb"
        );
//        Log.d("TESTANDROID", "rasdfafa");
//        Iterator<Operation> ops = tensorflow.graph().operations();
//        while(ops.hasNext()){
//            Operation op = ops.next();
//            Log.d("TESTANDROID", "tensorflowTest: " + op.name());
//        }

        float[] input = {0, 0, 0, 0};
        tensorflow.feed("input_input", input, 1, input.length);
        tensorflow.run(new String[]{"output/Softmax"});
        float output[] = new float[3];
        tensorflow.fetch("output/Softmax", output);
        Log.d("TESTANDROID", "tensorflowTest: " + Arrays.toString(output));
    }
}
