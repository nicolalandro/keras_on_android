import keras
import pandas as pd
from keras.layers import Dense
from keras.models import Sequential, save_model
from sklearn.metrics import accuracy_score


CSV_COLUMN_NAMES = ['SepalLength', 'SepalWidth',
                    'PetalLength', 'PetalWidth', 'Species']
SPECIES = ['Setosa', 'Versicolor', 'Virginica']

train_csv = pd.read_csv('iris_training.csv', names=CSV_COLUMN_NAMES)
train_examples = train_csv.drop(['Species'], axis=1).values
train_truths = train_csv['Species'].get_values()

test_csv = pd.read_csv('iris_test.csv', names=CSV_COLUMN_NAMES)
test_examples = test_csv.drop(['Species'], axis=1).values
test_truths = test_csv['Species'].get_values()

model = Sequential()
model.add(Dense(units=32, activation='relu', input_dim=4, name='input'))
model.add(Dense(units=16, activation='relu', input_dim=4))
model.add(Dense(units=3, activation='softmax', name='output'))
model.compile(loss=keras.losses.sparse_categorical_crossentropy,
              optimizer=keras.optimizers.SGD(lr=0.01, momentum=0.9, nesterov=True))

model.fit(train_examples, train_truths, epochs=40, batch_size=128, verbose=0)

# Evaluate
loss_and_metrics = model.evaluate(test_examples, test_truths, batch_size=128)
print("loss and metrics: ", loss_and_metrics)
prediction = model.predict(test_examples, batch_size=128)
prediction = prediction.argmax(1)
print("result with neural network: ", accuracy_score(test_truths, prediction))

save_model(model, 'keras.h5')
