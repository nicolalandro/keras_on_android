import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from tensorflow.python.tools import freeze_graph, optimize_for_inference_lib

model = load_model('keras.h5')
model_name = "keras"
output_node_name = "output/Softmax"  # guardare come lo ho chiamato nel create
input_node_name = "input_input"  # guardare come lo ho chiamato nel create

tf.train.write_graph(
    K.get_session().graph_def,
    'out',
    model_name + '_graph.pbtxt'
)

tf.train.Saver().save(
    K.get_session(),
    'out/' + model_name + '.chkp'
)

freeze_graph.freeze_graph(
    'out/' + model_name + '_graph.pbtxt',
    None,
    False,
    'out/' + model_name + '.chkp',
    output_node_name,
    "save/restore_all",
    "save/Const:0",
    'out/frozen_' + model_name + '.pb',
    True,
    ""
)

input_graph_def = tf.GraphDef()
with tf.gfile.Open('out/frozen_' + model_name + '.pb', "rb") as f:
    input_graph_def.ParseFromString(f.read())

output_graph_def = optimize_for_inference_lib.optimize_for_inference(
    input_graph_def,
    [input_node_name],
    [output_node_name],
    tf.float32.as_datatype_enum
)

with tf.gfile.FastGFile('out/tensorflow_lite_' + model_name + '.pb', "wb") as f:
    f.write(output_graph_def.SerializeToString())
